*
*******************************************
*
*PMEG120G10ELR
*
*NEXPERIA Germany GmbH
*
*120V, 1A low leakage current SiGe MEGA Schottky barrier rectifier
*
*
*VRmax     = 120V
*
*IFmax(AV) = 1A 
*VF        = 770mV @ IF = 1A
*IR        = 0.2nA @ VR = 120V
*
*
*
*
*
*
*
*
*
*Package pinning does not match Spice model pinning.
*Package: CFP3 (SOD123W)
*
*Package Pin 1: cathode 
*Package Pin 2: anode 
* 
*
*
*Extraction date (week/year): 35/2019
*Simulator: SPICE3
*
*******************************************
*#
.SUBCKT PMEG120G10ELR 1 2
R1 1 2 1E+13
D1 1 2
+ DIODE1
D2 1 2
+ DIODE2
*
*The resistor R1 and the diode D2  
*do not reflect physical devices  
*but improve only modeling in the  
*reverse mode of operation.
*
.MODEL DIODE1 D
+ IS = 5E-15
+ N = 0.82
+ BV = 129
+ IBV = 0.03
+ RS = 0.09
+ CJO = 5.7E-11
+ VJ = 0.65
+ M = 0.46
+ FC = 0.5
+ TT = 0
+ EG = 0.69
+ XTI = 2
.MODEL DIODE2 D
+ IS = 5E-12
+ N = 1
+ RS = 1.2
.ENDS
*








