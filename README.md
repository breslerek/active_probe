# active_probe

Oscilloscope active probe project realised for Impulse Techniques lecture from Faculty of Electronics
and Information Technology at Warsaw University of Technology. 

Whole project was realised under watchful eye of  PhD [Aleksander Burd](http://burd.pl/), to whom I am ever grateful for all supporting comments and reviews. Without any doubt Mr. Burd is one of the best lecturers I have met in my life, as well as a great electronics engineer. 

The main idea of active probe is based on two pairs of NPN and PNP emitter followers (QWT in short):
![](images/qwt.PNG)
Such configuration allows to preserve both falling and rising edges of probed signal. Two devices were designed:
- First one consisting of JFET follower and QWT, to provide high input impedance for signals up to 100MHz. 
- Second one consisitng only of QWT, to maximize possible bandwith. 

Two prtotypes of second option were built: 
- One based on transistors from BC family as a proof of concept, 
- One base on HF transistors with low capaticances, which allow for higher bandwith. 

Both prototypes looked as follows:
![](images/active_probe_no_case.JPG)
![](images/active_probe.JPG)

Bandwith of first prtotype was 170MHz:
![](images/bc_band.png)

Bandwith of HF prtotype was 868MHz: 
![](images/rf_band.png)

The signal is biased with reference to local ground. It is a result of large difference of betas in used transistors. The fix would require to change resistor used to bias transistor to bigger ones or to create a path for excessive base current. It has not yet been implemented. 

Full project report is available in .pdf, but it is written in Polish (maybe one day I will try to make an english copy :P).
